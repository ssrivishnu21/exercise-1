

public class Farm {
    /*
    This is a class representing a rectangular farm.
    It has getArea() method returning area of the farm.
     */
    private double length;
    private double breadth;

    //This is the constructor for initializing the length and breadth
    Farm(double length, double breadth) {
        /*
        @param : length
        @param : breadth
         */
        this.length = length;
        this.breadth = breadth;
    }

    //getArea method is used to return the area of the farm.
    public double getArea() {
        double area= length * breadth;
        if(area<=0){
            throw new IllegalArgumentException("Dimension should not be negative");
        }
        return area;
    }

    public double getPerimeter() {
        double perimeter= 2 * (length + breadth);
        if(perimeter<=0){
            throw new IllegalArgumentException("Dimension should not be negative");
        }
        return perimeter;
    }
}
